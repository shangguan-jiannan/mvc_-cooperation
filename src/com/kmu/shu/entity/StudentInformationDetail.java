package com.kmu.shu.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class StudentInformationDetail implements Serializable {
    private String studentNo;
    private String studentName;
    private byte gender;
    private Date birthday;
    private Integer totalScore;
    private String remark;
    private MajorDetail majorDetail;
    private Set course = new HashSet();

    public StudentInformationDetail() {
    }

    public StudentInformationDetail(String studentNo, String studentName, byte gender) {
        this.studentNo = studentNo;
        this.studentName = studentName;
        this.gender = gender;
    }

    public StudentInformationDetail(String studentNo, String studentName, byte gender, Date birthday, Integer totalScore, String remark, MajorDetail majorDetail) {
        this.studentNo = studentNo;
        this.studentName = studentName;
        this.gender = gender;
        this.birthday = birthday;
        this.totalScore = totalScore;
        this.remark = remark;
        this.majorDetail = majorDetail;
    }

    public Set getCourse() {
        return course;
    }

    public void setCourse(Set course) {
        this.course = course;
    }

    public MajorDetail getMajorDetail() {
        return majorDetail;
    }

    public void setMajorDetail(MajorDetail majorDetail) {
        this.majorDetail = majorDetail;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public byte getGender() {
        return gender;
    }

    public void setGender(byte gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StudentInformationDetail that = (StudentInformationDetail) o;
        return gender == that.gender && Objects.equals(studentNo, that.studentNo) && Objects.equals(studentName, that.studentName) && Objects.equals(birthday, that.birthday) && Objects.equals(totalScore, that.totalScore) && Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentNo, studentName, gender, birthday, totalScore, remark);
    }
}
