package com.kmu.shu.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CourseDetail {
    private String courseId;
    private String courseName;
    private Short schoolTerm;
    private Integer classHour;
    private Integer credit;
    private Set students = new HashSet();

    public CourseDetail() {
    }

    public CourseDetail(String courseId, String courseName, Short schoolTerm, Integer classHour, Integer credit, Set students) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.schoolTerm = schoolTerm;
        this.classHour = classHour;
        this.credit = credit;
        this.students = students;
    }

    public Set getStudents() {
        return students;
    }

    public void setStudents(Set students) {
        this.students = students;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Short getSchoolTerm() {
        return schoolTerm;
    }

    public void setSchoolTerm(Short schoolTerm) {
        this.schoolTerm = schoolTerm;
    }

    public Integer getClassHour() {
        return classHour;
    }

    public void setClassHour(Integer classHour) {
        this.classHour = classHour;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseDetail that = (CourseDetail) o;
        return Objects.equals(courseId, that.courseId) && Objects.equals(courseName, that.courseName) && Objects.equals(schoolTerm, that.schoolTerm) && Objects.equals(classHour, that.classHour) && Objects.equals(credit, that.credit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, courseName, schoolTerm, classHour, credit);
    }
}
