package com.kmu.shu.entity;

import java.util.Objects;

public class MajorDetail {
    private int majorId;
    private String majorName;
    private Integer peopleNumber;
    private String leader;

    public int getId() {
        return majorId;
    }

    public void setId(int id) {
        this.majorId = id;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public Integer getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(Integer peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MajorDetail that = (MajorDetail) o;
        return majorId == that.majorId && Objects.equals(majorName, that.majorName) && Objects.equals(peopleNumber, that.peopleNumber) && Objects.equals(leader, that.leader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(majorId, majorName, peopleNumber, leader);
    }
}
