package com.kmu.shu.entity;

import java.io.Serializable;
import java.util.Objects;

public class ConnectionDetailPK implements Serializable {
    private String studentNo;
    private String courseId;

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectionDetailPK that = (ConnectionDetailPK) o;
        return Objects.equals(studentNo, that.studentNo) && Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentNo, courseId);
    }
}
