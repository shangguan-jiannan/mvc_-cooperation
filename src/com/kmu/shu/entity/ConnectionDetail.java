package com.kmu.shu.entity;

import java.io.Serializable;
import java.util.Objects;

public class ConnectionDetail implements Serializable {
    private String studentNo;
    private String courseId;

    public ConnectionDetail() {
    }

    public ConnectionDetail(String studentNo, String courseId) {
        this.studentNo = studentNo;
        this.courseId = courseId;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectionDetail that = (ConnectionDetail) o;
        return Objects.equals(studentNo, that.studentNo) && Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentNo, courseId);
    }
}
