package com.kmu.shu.controller;

import com.kmu.shu.dao.LoginDao;
import com.kmu.shu.dao.impl.LoginDaoImp;
import com.kmu.shu.entity.LoginDetail;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

public class LoginController extends ActionSupport {

    private LoginDetail loginDetail;

    public LoginDetail getLoginDetail() {
        return loginDetail;
    }

    public void setLoginDetail(LoginDetail loginDetail) {
        this.loginDetail = loginDetail;
    }

    @Override
    public String execute() throws Exception {
        LoginDao loginDao = new LoginDaoImp();
        LoginDetail user = loginDao.validateUser(loginDetail.getStudentNo(), loginDetail.getToken());
        if(user != null){
            Map session=(Map) ActionContext.getContext().getSession();
            session.put("user", user);
            return "success";
        }else{
            return "error";
        }
    }
}
