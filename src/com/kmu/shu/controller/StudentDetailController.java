package com.kmu.shu.controller;

import com.kmu.shu.dao.MajorDetailDao;
import com.kmu.shu.dao.StudentInformationDetailDao;
import com.kmu.shu.dao.impl.CourseDetailDaoImp;
import com.kmu.shu.dao.impl.MajorDetailDaoImp;
import com.kmu.shu.dao.impl.StudentInformationDetailDaoImp;
import com.kmu.shu.entity.CourseDetail;
import com.kmu.shu.entity.LoginDetail;
import com.kmu.shu.entity.MajorDetail;
import com.kmu.shu.entity.StudentInformationDetail;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StudentDetailController extends ActionSupport {

    StudentInformationDetailDao studentInformationDetailDao;
    private StudentInformationDetail studentInformationDetail;
    private CourseDetail courseDetail;
    private MajorDetail majorDetail;

    public StudentInformationDetail getStudentInformationDetail() {
        return studentInformationDetail;
    }

    public void setStudentInformationDetail(StudentInformationDetail studentInformationDetail) {
        this.studentInformationDetail = studentInformationDetail;
    }

    public CourseDetail getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(CourseDetail courseDetail) {
        this.courseDetail = courseDetail;
    }

    public MajorDetail getMajorDetail() {
        return majorDetail;
    }

    public void setMajorDetail(MajorDetail majorDetail) {
        this.majorDetail = majorDetail;
    }
    //查询学生个人信息
    @Override
    public String execute() throws Exception {
        Map session=(Map) ActionContext.getContext().getSession();
        LoginDetail user=(LoginDetail)session.get("user");
        studentInformationDetailDao=new StudentInformationDetailDaoImp();
        StudentInformationDetail xs=studentInformationDetailDao.getInfo(user.getStudentNo());
        Map request=(Map)ActionContext.getContext().get("request");
        request.put("xs", xs);//request对象的put方法功能为使用request对象向jsp上传数据/值（xs）
        return "success";
    }


    //进入修改学生信息页面
    public String updateXsInfo() throws Exception{
        //获取当前用户对象
        Map session=(Map)ActionContext.getContext().getSession();
        LoginDetail user=(LoginDetail)session.get("user");
        studentInformationDetailDao=new StudentInformationDetailDaoImp();
        MajorDetailDao zyDao=new MajorDetailDaoImp();
        List zys=zyDao.getAll();
        //得到当前学生的信息
        StudentInformationDetail xs=studentInformationDetailDao.getInfo(user.getStudentNo());
        Map request=(Map)ActionContext.getContext().get("request");
        request.put("zys", zys);
        request.put("xs", xs);
        return SUCCESS;
    }

    public String updateXs() throws Exception{
        studentInformationDetailDao=new StudentInformationDetailDaoImp();
        MajorDetailDao zyDao=new MajorDetailDaoImp();
        StudentInformationDetail stu=new StudentInformationDetail();
        stu.setStudentNo(studentInformationDetail.getStudentNo());
        stu.setStudentName(studentInformationDetail.getStudentName());
        stu.setGender(studentInformationDetail.getGender());
        stu.setBirthday(studentInformationDetail.getBirthday());
        stu.setTotalScore(studentInformationDetail.getTotalScore());
        stu.setRemark(studentInformationDetail.getRemark());

        MajorDetail zy=zyDao.getInfo(majorDetail.getId());
        stu.setMajorDetail(zy);
        //由于没有修改学生对应的选修的课程，所以直接取出不用改变
        //Hibernate 级联到第三张表，所以要设置，如果不设置，会认为设置为空，会把连接表中有关内容删除
        Set list=studentInformationDetailDao.getInfo(studentInformationDetail.getStudentNo()).getCourse();
        //设置学生对应多项课程的 Set
        stu.setCourse(list);
        //修改
        studentInformationDetailDao.update(stu);
        return SUCCESS;
    }

    public String getXsKcs() throws Exception{
        Map session=(Map)ActionContext.getContext().getSession();
        LoginDetail user=(LoginDetail)session.get("user");
        String studentNo=user.getStudentNo();
        //得到当前学生的信息
        StudentInformationDetail xsb=new StudentInformationDetailDaoImp().getInfo(studentNo);
        //取出选修的课程 Set
        Set list=xsb.getCourse();
        Map request=(Map)ActionContext.getContext().get("request");
        //保存
        request.put("list",list);
        return SUCCESS;
    }

    //退选课程
    public String deleteKc() throws Exception{
        Map session=(Map)ActionContext.getContext().getSession();
        String xh=((LoginDetail)session.get("user")).getStudentNo();
        studentInformationDetailDao=new StudentInformationDetailDaoImp();
        StudentInformationDetail xs2=studentInformationDetailDao.getInfo(xh);
        Set list=xs2.getCourse();
        Iterator iter=list.iterator();
        while(iter.hasNext()){
            CourseDetail kc2=(CourseDetail)iter.next();
            //如果遍历到退选的课程的课程号就从list中删除
            if(kc2.getCourseId().equals(courseDetail.getCourseId())){
                iter.remove();
            }
        }
        //设置课程的 Set
        xs2.setCourse(list);
        studentInformationDetailDao.update(xs2);
        return SUCCESS;
    }

    //选定课程
    public String selectKc() throws Exception{
        Map session=(Map)ActionContext.getContext().getSession();
        String xh=((LoginDetail)session.get("user")).getStudentNo();
        studentInformationDetailDao=new StudentInformationDetailDaoImp();
        StudentInformationDetail xs3=studentInformationDetailDao.getInfo(xh);
        Set list=xs3.getCourse();
        Iterator iter=list.iterator();
        //选修课程时先遍历已经选的课程，如果在已经选修的课程中找到就返回 ERROR
        while(iter.hasNext()){
            CourseDetail kc3=(CourseDetail)iter.next();
            if(kc3.getCourseId().equals(courseDetail.getCourseId())){
                return ERROR;
            }
        }
        //如果没找到，就添加到集合中
        list.add(new CourseDetailDaoImp().getInfo(courseDetail.getCourseId()));
        xs3.setCourse(list);
        studentInformationDetailDao.update(xs3);
        return SUCCESS;
    }
}
