package com.kmu.shu.controller;

import com.kmu.shu.dao.CourseDetailDao;
import com.kmu.shu.dao.impl.CourseDetailDaoImp;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.List;
import java.util.Map;

public class CourseDetailController extends ActionSupport {

    @Override
    public String execute()throws Exception{
        CourseDetailDao kcDao=new CourseDetailDaoImp();
        List list=kcDao.getAll();
        Map request=(Map) ActionContext.getContext().get("request");
        request.put("list", list);
        return SUCCESS;
    }
}
