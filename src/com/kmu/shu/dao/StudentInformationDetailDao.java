package com.kmu.shu.dao;

import com.kmu.shu.entity.StudentInformationDetail;

public interface StudentInformationDetailDao {

    public StudentInformationDetail getInfo(String studentNo);

    public void update(StudentInformationDetail studentInformationDetail);
}
