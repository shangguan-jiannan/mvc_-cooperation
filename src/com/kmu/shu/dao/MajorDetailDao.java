package com.kmu.shu.dao;

import com.kmu.shu.entity.MajorDetail;

import java.util.List;

public interface MajorDetailDao {

    public MajorDetail getInfo(Integer id);

    public List getAll();
}
