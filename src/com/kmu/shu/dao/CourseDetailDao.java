package com.kmu.shu.dao;

import com.kmu.shu.entity.CourseDetail;

import java.util.List;

public interface CourseDetailDao {

    public CourseDetail getInfo(String courseId);

    public List getAll();
}
