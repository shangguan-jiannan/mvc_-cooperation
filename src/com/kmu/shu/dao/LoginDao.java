package com.kmu.shu.dao;

import com.kmu.shu.entity.LoginDetail;

/**
 * @author lycan
 * 登录验证DAO
 */
public interface LoginDao {

    /**
     * 验证用户接口
     * @param studentNo 学生学号
     * @param token 秘钥
     * @return 用户实体对象
     */
    public LoginDetail validateUser(String studentNo, String token);

}
