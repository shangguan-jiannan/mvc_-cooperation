package com.kmu.shu.dao.impl;

import com.kmu.shu.dao.LoginDao;
import com.kmu.shu.entity.LoginDetail;
import com.kmu.shu.factory.HibernateFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class LoginDaoImp implements LoginDao {

    @Override
    public LoginDetail validateUser(String studentNo, String token) {

        Session session =HibernateFactory.getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from LoginDetail where studentNo = ?1 and token = ?2")
                .setParameter(1, studentNo)
                .setParameter(2, token)
                .setMaxResults(1);
        LoginDetail loginDetail = (LoginDetail) query.uniqueResult();
        transaction.commit();
        session.close();
        return loginDetail;
    }
}
