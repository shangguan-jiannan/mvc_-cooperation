package com.kmu.shu.dao.impl;

import com.kmu.shu.dao.StudentInformationDetailDao;
import com.kmu.shu.entity.StudentInformationDetail;
import com.kmu.shu.factory.HibernateFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class StudentInformationDetailDaoImp implements StudentInformationDetailDao {

    @Override
    public StudentInformationDetail getInfo(String studentNo) {
        try{
            Session session= HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            Query query=session.createQuery("from StudentInformationDetail where studentNo=?1");
            query.setParameter(1, studentNo);
            query.setMaxResults(1);
            StudentInformationDetail xs=(StudentInformationDetail) query.uniqueResult();
            ts.commit();
            session.clear();
            return xs;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(StudentInformationDetail studentInformationDetail) {
        try{
            Session session=HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            session.update(studentInformationDetail);
            ts.commit();
            session.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
