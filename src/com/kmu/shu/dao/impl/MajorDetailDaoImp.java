package com.kmu.shu.dao.impl;

import com.kmu.shu.dao.MajorDetailDao;
import com.kmu.shu.entity.MajorDetail;
import com.kmu.shu.factory.HibernateFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class MajorDetailDaoImp implements MajorDetailDao {
    @Override
    public MajorDetail getInfo(Integer id) {
        try{
            Session session=HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            Query query=session.createQuery("from MajorDetail where id=?1");
            query.setParameter(1, id);
            query.setMaxResults(1);
            MajorDetail majorDetail=(MajorDetail) query.uniqueResult();
            ts.commit();
            session.close();
            return majorDetail;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List getAll() {
        try{
            Session session=HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            List list=session.createQuery("from MajorDetail ").list();
            ts.commit();
            session.close();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
