package com.kmu.shu.dao.impl;

import com.kmu.shu.dao.CourseDetailDao;
import com.kmu.shu.entity.CourseDetail;
import com.kmu.shu.factory.HibernateFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CourseDetailDaoImp implements CourseDetailDao {
    @Override
    public CourseDetail getInfo(String courseId) {
        try{
            Session session=HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            Query query=session.createQuery("from CourseDetail where courseId=?1");
            query.setParameter(1, courseId);
            query.setMaxResults(1);
            CourseDetail courseDetail=(CourseDetail) query.uniqueResult();
            ts.commit();
            session.clear();
            return courseDetail;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List getAll() {
        try{
            Session session=HibernateFactory.getSession();
            Transaction ts=session.beginTransaction();
            List list=session.createQuery("from CourseDetail order by courseId").list();
            ts.commit();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
