<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
	<title>学生选课系统</title>
</head>
<body bgcolor="#D9DFAA">
	<table width="400">
		<s:set value="#request.xs" var="xs"/>
		<tr>
			<td>学号：</td>
			<td><s:property value="#xs.studentNo"/></td>
		</tr>
		<tr>
			<td>姓名：</td>
			<td><s:property value="#xs.studentName"/></td>
		</tr>
		<tr>
			<td>性别：</td>
			<td>
				<s:if test="#xs.gender==1">男</s:if>
				<s:else>女</s:else>
			</td>
		</tr>
		<tr>
			<td>专业：</td>
			<td><s:property value="#xs.zyb.majorName"/></td>
		</tr>
		<tr>
			<td>出生时间：</td>
			<td><s:property value="#xs.birthday"/></td>
		</tr>
		<tr>
			<td>总学分：</td>
			<td><s:property value="#xs.totalScore"/></td>
		</tr>
		<tr>
			<td>备注：</td>
			<td><s:property value="#xs.remark"/></td>
		</tr>
	</table>
</body>
</html>
