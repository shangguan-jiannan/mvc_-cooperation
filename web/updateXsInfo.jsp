<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
	<title>学生选课系统</title>
</head>
<body bgcolor="#D9DFAA">
	<s:set var="xs" value="#request.studentInformationDetail"></s:set>
	<!--上传文件时要加入黑体部分-->
	<s:form action="updateXs" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td>学号:</td>
			<td>
				<input type="text" name="xs.studentNo" value="<s:property value="#xs.studentNo"/>" readOnly/>
			</td>
		</tr>
		<tr>
			<td>姓名:</td>
			<td>
				<input type="text" name="xs.studentName" value="<s:property value="#xs.studentName"/>" />
			</td>
		</tr>
		<tr>
			<s:radio list="#{1:'男',0:'女'}" value="#xs.gender" label="性别" name="xs.gender"></s:radio>
		</tr>
		<tr>
			<td>专业:</td>
			<td>
				<!--遍历出专业的信息-->
				<select name="zyb.id">
					<s:iterator id="zy" value="#request.zys">
						<option value="<s:property value="#zy.id"/>">
							<s:property value="#zy.majorName"/>
						</option>
					</s:iterator>
				</select>
			</td>
		</tr>
		<tr>
			<td>出生时间:</td>
			<td>
				<input type="text" name="xs.birthday" value="<s:property value="#xs.birthday"/>"/>
			</td>
		</tr>
		<tr>
			<td>备注:</td>
			<td>
				<input type="text" name="xs.remark" value="<s:property value="#xs.remark"/>" />
			</td>
		</tr>
		<tr>
			<td>总学分:</td>
			<td>
				<input type="text" name="xs.totalScore" value="<s:property value="#xs.totalScore"/>" />
			</td>
		</tr>
		<tr>
			<td><input type="submit" value="修改"/></td>
		</tr>
	</table>
	</s:form>
</body>
</html>