<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
	<title>学生选课系统</title>
</head>
<body>
	<s:form action="login" method="post">
		<table>
			<tr>
				<s:textfield name="loginDetail.studentNo" label="学号" size="20"></s:textfield>
			</tr>
			<tr>
				<s:password name="loginDetail.token" label="口令" size="22"></s:password>
			</tr>
			<tr>
				<td align="left"><input type="submit" value="登录" /></td>
				<td><input type="reset" value="重置" /></td>
			</tr>
		</table>
	</s:form>
</body>
</html>
